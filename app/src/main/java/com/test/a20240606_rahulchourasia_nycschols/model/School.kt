package com.test.a20240606_rahulchourasia_nycschols.model

data class School(var dbn : String, var school_name : String, var overview_paragraph : String,
                  var city : String, var state_code : String, var language_classes : String,
                  var phone_number : String, var website : String, var school_email : String,
                  var program1 : String, var start_time : String, var end_time : String) {
}