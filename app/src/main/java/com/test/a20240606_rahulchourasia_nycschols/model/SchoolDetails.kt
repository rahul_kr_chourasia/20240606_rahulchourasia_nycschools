package com.test.a20240606_rahulchourasia_nycschols.model

data class SchoolDetails(var dbn : String, var num_of_sat_test_takers : String,
                         var sat_critical_reading_avg_score : String,
                         var sat_math_avg_score : String, var sat_writing_avg_score : String) {
}