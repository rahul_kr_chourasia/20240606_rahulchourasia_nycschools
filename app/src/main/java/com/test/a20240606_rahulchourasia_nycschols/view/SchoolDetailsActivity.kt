package com.test.a20240606_rahulchourasia_nycschols.view

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.test.a20240606_rahulchourasia_nycschols.R
import com.test.a20240606_rahulchourasia_nycschols.databinding.ActivitySchoolDetailsBinding
import com.test.a20240606_rahulchourasia_nycschols.model.School
import com.test.a20240606_rahulchourasia_nycschols.model.SchoolDetails
import com.test.a20240606_rahulchourasia_nycschols.network.Repository
import com.test.a20240606_rahulchourasia_nycschols.network.RetrofitService
import com.test.a20240606_rahulchourasia_nycschols.viewmodel.MyViewModelFactory
import com.test.a20240606_rahulchourasia_nycschols.viewmodel.SchoolDetailsViewModel

class SchoolDetailsActivity : AppCompatActivity() {
    private val TAG = "SchoolDetailsActivity"
    private lateinit var binding: ActivitySchoolDetailsBinding

    private lateinit var viewModel: SchoolDetailsViewModel
    private val retrofitService = RetrofitService.getInstance()
    private lateinit var dataFromList : Intent
    val schoolMap: HashMap<String, SchoolDetails> = HashMap()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_details)
        binding = ActivitySchoolDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dataFromList = intent
        var dbn = dataFromList.getStringExtra(getString(R.string.dbn))
        binding.tvSchoolName.text = dataFromList.getStringExtra(getString(R.string.school_name))
        binding.tvSchoolDesc.text = dataFromList.getStringExtra(getString(R.string.school_desc))
        binding.tvSchoolCity.text = dataFromList.getStringExtra(getString(R.string.school_city))
        binding.tvSchoolStateCode.text = dataFromList.getStringExtra(getString(R.string.school_state))

        binding.ivIconCall.setOnClickListener{ callSchool() }
        binding.ivIconMail.setOnClickListener{ mailSchool() }
        binding.ivIconWebsite.setOnClickListener{ openSchoolWebsite() }
        binding.ivIconBack.setOnClickListener{finish()}

        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory(Repository(retrofitService))
        )[SchoolDetailsViewModel::class.java]

        viewModel.schoolDetails.observe(this, Observer { schoolList ->
            Log.d(TAG, "onCreate: $schoolList")
            for (school in schoolList) {
                schoolMap[school.dbn] = school
            }

            if (schoolMap.contains(dbn)){
                val school = schoolMap[dbn]
                binding.tvMathScore.text = school?.sat_math_avg_score
                binding.tvWriteScore.text = school?.sat_writing_avg_score
                binding.tvReadingScore.text = school?.sat_critical_reading_avg_score
            }else{
                binding.tvMathScore.text = "N/A"
                binding.tvWriteScore.text = "N/A"
                binding.tvReadingScore.text = "N/A"
            }

        })

        viewModel.errorMessage.observe(this, Observer {

        })

        viewModel.getSchoolDetails()
    }

    private fun callSchool(){
        val phoneNumber = dataFromList.getStringExtra(getString(R.string.school_phone))
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phoneNumber"))

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CALL_PHONE)
        } else {
            startActivity(intent)
        }
    }

    private fun mailSchool(){
        var intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:")
        intent.putExtra(Intent.EXTRA_EMAIL, dataFromList.getStringExtra(getString(R.string.school_email)))
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent);
        }
    }

    private fun openSchoolWebsite(){
        try {
            val websiteUrl = dataFromList.getStringExtra(getString(R.string.school_website))
            if (!websiteUrl.isNullOrEmpty()) {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(websiteUrl))
                startActivity(browserIntent)
            } else {
                Toast.makeText(this, "Website URL is missing", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Failed to open the website", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CALL_PHONE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission granted, make the call
                    val phoneNumber = dataFromList.getStringExtra(getString(R.string.school_phone))
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$phoneNumber"))
                    startActivity(intent)
                } else {
                    // Permission denied, show a message to the user
                    Toast.makeText(this, "Call permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    companion object {
        private const val REQUEST_CALL_PHONE = 1
    }
}