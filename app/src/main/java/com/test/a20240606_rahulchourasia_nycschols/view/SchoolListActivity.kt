package com.test.a20240606_rahulchourasia_nycschols.view

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.a20240606_rahulchourasia_nycschols.R
import com.test.a20240606_rahulchourasia_nycschols.adapter.SchoolListAdapter
import com.test.a20240606_rahulchourasia_nycschols.databinding.ActivitySchoolListBinding
import com.test.a20240606_rahulchourasia_nycschols.network.Repository
import com.test.a20240606_rahulchourasia_nycschols.network.RetrofitService
import com.test.a20240606_rahulchourasia_nycschols.viewmodel.SchoolListViewModel
import com.test.a20240606_rahulchourasia_nycschols.viewmodel.MyViewModelFactory

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    private lateinit var binding: ActivitySchoolListBinding

    private lateinit var viewModel: SchoolListViewModel
    private lateinit var progressDialog: ProgressDialog

    private val retrofitService = RetrofitService.getInstance()
    private val adapter = SchoolListAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_list)
        binding = ActivitySchoolListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        progressDialog = ProgressDialog(this).apply {
            setMessage("Loading...")
            setCancelable(false)
        }

        viewModel = ViewModelProvider(
            this,
            MyViewModelFactory(Repository(retrofitService))
        )[SchoolListViewModel::class.java]

        binding.rvSchoolList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        binding.rvSchoolList.adapter = adapter

        viewModel.schoolList.observe(this, Observer {
            Log.d(TAG, "onCreate: $it")
            adapter.setSchoolList( it, this@MainActivity)
            progressDialog.dismiss()
        })

        viewModel.errorMessage.observe(this, Observer {
            progressDialog.dismiss()
        })

        progressDialog.show()
        viewModel.getAllSchools()

        binding.etSearchSchool.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapter.filter(s.toString())
            }
        })

    }
}