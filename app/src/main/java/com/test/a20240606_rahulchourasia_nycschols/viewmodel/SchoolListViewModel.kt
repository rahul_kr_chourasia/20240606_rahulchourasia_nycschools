package com.test.a20240606_rahulchourasia_nycschols.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.a20240606_rahulchourasia_nycschols.model.School
import com.test.a20240606_rahulchourasia_nycschols.network.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SchoolListViewModel constructor(private val repository: Repository)  : ViewModel() {

    val schoolList = MutableLiveData<List<School>>()
    val errorMessage = MutableLiveData<String>()

    fun getAllSchools() {

        val response = repository.getAllSchools()
        response.enqueue(object : Callback<List<School>> {
            override fun onResponse(call: Call<List<School>>, response: Response<List<School>>) {
                schoolList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<School>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }


}