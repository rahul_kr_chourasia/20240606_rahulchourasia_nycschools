package com.test.a20240606_rahulchourasia_nycschols.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.a20240606_rahulchourasia_nycschols.model.SchoolDetails
import com.test.a20240606_rahulchourasia_nycschols.network.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SchoolDetailsViewModel constructor(private val repository: Repository)  : ViewModel() {

    val schoolDetails = MutableLiveData<List<SchoolDetails>>()
    val errorMessage = MutableLiveData<String>()

    fun getSchoolDetails() {
        val response = repository.getSATScores()
        response.enqueue(object : Callback<List<SchoolDetails>> {
            override fun onResponse(call: Call<List<SchoolDetails>>, response: Response<List<SchoolDetails>>) {
                schoolDetails.postValue(response.body())
            }

            override fun onFailure(call: Call<List<SchoolDetails>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }
}