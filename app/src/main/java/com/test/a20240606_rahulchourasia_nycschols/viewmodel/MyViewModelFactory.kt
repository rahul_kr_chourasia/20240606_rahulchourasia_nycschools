package com.test.a20240606_rahulchourasia_nycschols.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.a20240606_rahulchourasia_nycschols.network.Repository

class MyViewModelFactory constructor(private val repository: Repository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when{
            modelClass.isAssignableFrom(SchoolListViewModel::class.java) ->
                SchoolListViewModel(this.repository) as T
            modelClass.isAssignableFrom(SchoolDetailsViewModel::class.java) ->
                SchoolDetailsViewModel(this.repository) as T
            else ->
                throw IllegalArgumentException("ViewModel Not Found")

        }

    }
}