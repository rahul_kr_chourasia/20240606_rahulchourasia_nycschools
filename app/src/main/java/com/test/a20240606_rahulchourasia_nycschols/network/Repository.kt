package com.test.a20240606_rahulchourasia_nycschols.network

import com.test.a20240606_rahulchourasia_nycschols.model.School

class Repository constructor(private val retrofitService: RetrofitService) {

    fun getAllSchools() = retrofitService.getAllSchools()
    fun getSATScores() = retrofitService.getSATScores()
}