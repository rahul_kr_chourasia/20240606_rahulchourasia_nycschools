package com.test.a20240606_rahulchourasia_nycschols.network

import com.test.a20240606_rahulchourasia_nycschols.model.School
import com.test.a20240606_rahulchourasia_nycschols.model.SchoolDetails
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetrofitService {
    @GET("s3k6-pzi2.json")
    fun getAllSchools(): Call<List<School>>

    @GET("f9bf-2cp4.json")
    fun getSATScores(): Call<List<SchoolDetails>>

    companion object {

        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://data.cityofnewyork.us/resource/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}