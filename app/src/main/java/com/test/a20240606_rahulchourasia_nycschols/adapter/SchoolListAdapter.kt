package com.test.a20240606_rahulchourasia_nycschols.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.a20240606_rahulchourasia_nycschols.R
import com.test.a20240606_rahulchourasia_nycschols.databinding.RvItemSchoolsBinding
import com.test.a20240606_rahulchourasia_nycschols.model.School
import com.test.a20240606_rahulchourasia_nycschols.view.SchoolDetailsActivity

class SchoolListAdapter: RecyclerView.Adapter<MainViewHolder>() {

    private var schools = mutableListOf<School>()
    private var mContext : Context? = null
    private var filteredSchools = mutableListOf<School>()

    fun setSchoolList(schools: List<School>, mContext: Context) {
        this.schools = schools.toMutableList()
        this.mContext = mContext
        this.filteredSchools = schools.toMutableList()
        notifyDataSetChanged()
    }

    fun filter(query: String) {
        filteredSchools = if (query.isEmpty()) {
            schools.toMutableList()
        } else {
            schools.filter { it.school_name.contains(query, ignoreCase = true) }.toMutableList()
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RvItemSchoolsBinding.inflate(inflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        val school = filteredSchools[position]

        if (school.school_name.isNotBlank()) {
            holder.binding.tvSchoolName.text = school.school_name
            holder.binding.tvSchoolName.visibility = View.VISIBLE
        } else {
            holder.binding.tvSchoolName.visibility = View.GONE
        }

        if (school.city.isNotBlank() || school.state_code.isNotBlank()) {
            holder.binding.tvSchoolCity.text = school.city
            holder.binding.tvSchoolStateCode.text = school.state_code
            holder.binding.llAddress.visibility = View.VISIBLE
        } else {
            holder.binding.llAddress.visibility = View.GONE
        }

        if (school.program1.isNotBlank()) {
            holder.binding.tvSchoolProgram.text = school.program1
            holder.binding.tvSchoolProgram.visibility = View.VISIBLE
        } else {
            holder.binding.tvSchoolProgram.visibility = View.GONE
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(mContext, SchoolDetailsActivity::class.java)
            intent.putExtra(mContext?.getString(R.string.dbn), school.dbn)
            intent.putExtra(mContext?.getString(R.string.school_name), school.school_name)
            intent.putExtra(mContext?.getString(R.string.school_desc), school.overview_paragraph)
            intent.putExtra(mContext?.getString(R.string.school_email), school.school_email)
            intent.putExtra(mContext?.getString(R.string.school_phone), school.phone_number)
            intent.putExtra(mContext?.getString(R.string.school_website), school.website)
            intent.putExtra(mContext?.getString(R.string.school_city), school.city)
            intent.putExtra(mContext?.getString(R.string.school_state), school.state_code)
            mContext?.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return filteredSchools.size
    }
}

class MainViewHolder(val binding: RvItemSchoolsBinding) : RecyclerView.ViewHolder(binding.root)
